package com.example.examencalculadora;

import java.io.Serializable;

public class Calculadora implements Serializable {

    private float numA=0.0f, numB=0.0f;

    public Calculadora() {
    }

    public Calculadora(float numA, float numB) {
        this.numA = numA;
        this.numB = numB;
    }

    public float getNumA() {
        return numA;
    }

    public void setNumA(float numA) {
        this.numA = numA;
    }

    public float getNumB() {
        return numB;
    }

    public void setNumB(float numB) {
        this.numB = numB;
    }

    public float suma(){
        return numA + numB;
    }

    public float resta(){
        return numA - numB;
    }
    public float multiplicar(){
        return numA * numB;
    }

    public float division(){
        return numA / numB;

    }



}
